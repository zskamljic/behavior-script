package bs.parsing.errors;

import bs.lexing.Type;

public class ListTypeMismatchException extends ParsingException {
    public ListTypeMismatchException(Type requiredType, Type actualType) {
        super("List type mismatch, expected " + requiredType + ", but was " + actualType);
    }
}
