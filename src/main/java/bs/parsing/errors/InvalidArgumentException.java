package bs.parsing.errors;

public class InvalidArgumentException extends ParsingException {
    public InvalidArgumentException(String message) {
        super(message);
    }
}
