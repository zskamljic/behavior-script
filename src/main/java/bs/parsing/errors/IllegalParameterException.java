package bs.parsing.errors;

import bs.lexing.Type;

public class IllegalParameterException extends ParsingException {
    public IllegalParameterException(Type expected, Type actual) {
        super("Illegal argument: expected " + expected + ", given " + actual + " instead");
    }
}
