package bs.parsing.errors;

import bs.common.Keyword;

public class KeywordNotImplementedException extends ParsingException {
    public KeywordNotImplementedException(Keyword keyword) {
        super("Keyword " + keyword + " does not yet have an implementation.");
    }
}
