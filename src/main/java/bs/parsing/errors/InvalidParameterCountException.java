package bs.parsing.errors;

public class InvalidParameterCountException extends ParsingException {
    public InvalidParameterCountException(String keyword, int requiredParameters, int given) {
        super(keyword + " requires " + requiredParameters + " parameters, " + given + " given.");
    }
}
