package bs.parsing.errors;

import bs.common.Keyword;

public class InvalidContinuationException extends ParsingException {
    public InvalidContinuationException(Keyword keyword) {
        super("Keyword " + keyword + " was not terminated by new line.");
    }
}
