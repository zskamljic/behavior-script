package bs.parsing.errors;

import bs.lexing.Type;

public class IllegalListTypeParameter extends ParsingException {
    public IllegalListTypeParameter(Type type) {
        super(type + " is not a valid list element");
    }
}
