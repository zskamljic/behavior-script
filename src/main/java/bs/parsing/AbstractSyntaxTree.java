package bs.parsing;

import bs.common.Keyword;
import bs.common.Pair;
import bs.lexing.Symbol;
import bs.lexing.Type;
import bs.parsing.errors.*;
import bs.parsing.nodes.expressions.*;
import bs.parsing.nodes.statements.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AbstractSyntaxTree {
    public List<StatementNode> createTree(List<Symbol> symbols) throws ParsingException {
        return createStatementList(symbols);
    }

    List<StatementNode> createStatementList(List<Symbol> symbols) throws ParsingException {
        if (symbols.isEmpty()) return List.of();

        var statements = new ArrayList<StatementNode>();

        while (!symbols.isEmpty()) {
            statements.add(createStatement(symbols));
        }
        statements.removeIf(statement -> statement instanceof NoopStatement);

        return statements;
    }

    StatementNode createStatement(List<Symbol> symbols) throws ParsingException {
        var symbol = symbols.get(0);
        switch (symbol.type) {
            case KEYWORD:
                return createKeywordStatement(symbols);
            case METHOD_DECLARATION:
                return createMethodStatement(symbols);
            default:
                throw new ParsingException("Type of statement not yet supported: " + symbols.get(0).type +
                        ", value was: " + symbol.value);
        }
    }

    StatementNode createKeywordStatement(List<Symbol> symbols) throws ParsingException {
        var keywordSymbol = symbols.remove(0);
        var keyword = Keyword.getByCommand(keywordSymbol.value)
                .orElseThrow();

        switch (keyword) {
            case SAY:
                return createSayStatementNode(symbols);
            case WAIT:
                return createWaitStatementNode(symbols);
            case RANDOM:
                // If random is invoked as a statement we don't need to execute it as it would
                // pick a random value and do nothing with it, so we return a NO-OP statement,
                // symbols still need to be consumed

                createRandomExpressionNode(null, symbols);
                consumeNewLine(Keyword.RANDOM, symbols);
                return new NoopStatement();
            case INVOKE:
                return createMethodInvocationNode(symbols);
            default:
                throw new KeywordNotImplementedException(keyword);
        }
    }

    MethodStatement createMethodStatement(List<Symbol> symbols) throws ParsingException {
        var nameSymbol = symbols.remove(0);
        if (symbols.get(0).type == Type.LINE_TERMINATOR) {
            symbols.remove(0);
        }

        var methodSymbols = new ArrayList<Symbol>();
        var iterator = symbols.iterator();
        while (iterator.hasNext()) {
            var symbol = iterator.next();
            iterator.remove();
            if (symbol.type == Type.CONTROL_TERMINATOR) {
                break;
            }
            methodSymbols.add(symbol);
        }
        var statements = createStatementList(methodSymbols);
        symbols.remove(0);
        return new MethodStatement(nameSymbol.value, statements);
    }

    StatementNode createSayStatementNode(List<Symbol> symbols) throws ParsingException {
        if (symbols.isEmpty()) throw new InvalidParameterCountException(Keyword.SAY.getCommand(), 1, 0);

        var stringExpression = createStringExpression(symbols);
        consumeNewLine(Keyword.SAY, symbols);
        return new SayStatementNode(stringExpression);
    }

    private StringExpression createStringExpression(List<Symbol> symbols) throws ParsingException {
        var symbol = symbols.remove(0);
        switch (symbol.type) {
            case STRING_LITERAL:
                return new StringLiteralExpression(symbol.value);
            case VARIABLE:
                return new StringVariableExpression(symbol.value);
            case KEYWORD:
                if (symbol.value.equals(Keyword.RANDOM.getCommand())) {
                    return (StringExpression) createRandomExpressionNode(Type.STRING_LITERAL, symbols);
                }
                throw new InvalidArgumentException("Only \"random\" keyword can be used here.");
            default:
                throw new IllegalParameterException(Type.STRING_LITERAL, symbol.type);
        }
    }

    private StatementNode createMethodInvocationNode(List<Symbol> symbols) throws ParsingException {
        var symbol = symbols.remove(0);
        switch (symbol.type) {
            case IDENTIFIER:
                consumeNewLine(Keyword.INVOKE, symbols);
                return new InvocationStatement(symbol.value);
            case KEYWORD:
                if (symbol.value.equals(Keyword.RANDOM.getCommand())) {
                    var invocation = createRandomMethodInvocationNode(symbols);
                    consumeNewLine(Keyword.RANDOM, symbols);
                    return invocation;
                }
                throw new InvalidArgumentException("Only \"random\" keyword can be used here.");
            default:
                throw new IllegalParameterException(Type.IDENTIFIER, symbol.type);
        }
    }

    private StatementNode createRandomMethodInvocationNode(List<Symbol> symbols) throws ParsingException {
        var symbolsAndType = ensureRandomParametersPresent(Type.IDENTIFIER, symbols);
        return toRandomInvocation(symbolsAndType.getFirst(), symbolsAndType.getSecond());
    }

    private Pair<List<Symbol>, Type> ensureRandomParametersPresent(Type identifier, List<Symbol> symbols) throws ParsingException {
        if (symbols.isEmpty()) {
            throw new InvalidParameterCountException(Keyword.RANDOM.getCommand(), 1, 0);
        }

        var listStart = symbols.remove(0);
        if (listStart.type != Type.LIST_START) {
            throw new InvalidArgumentException("Invalid argument, expected list, but got " + listStart.type + " instead");
        }

        return createSymbolList(identifier, symbols);
    }

    private StatementNode toRandomInvocation(List<Symbol> symbols, Type type) throws ParsingException {
        var expressionList = new ArrayList<ValueExpression>();
        for (var symbol : symbols) {
            expressionList.add(toValueExpression(type, symbol));
        }

        if (!expressionList.stream().allMatch(expression -> expression instanceof IdentifierExpression)) {
            throw new InvalidArgumentException("Only method identifiers can be used here");
        }

        return new RandomInvocationStatement(expressionList);
    }

    StatementNode createWaitStatementNode(List<Symbol> symbols) throws ParsingException {
        if (symbols.isEmpty()) throw new InvalidParameterCountException(Keyword.WAIT.getCommand(), 1, 0);

        var integerExpression = createIntegerExpression(symbols);
        consumeNewLine(Keyword.WAIT, symbols);
        return new WaitStatementNode(integerExpression);
    }

    private IntegerExpression createIntegerExpression(List<Symbol> symbols) throws ParsingException {
        var symbol = symbols.remove(0);
        switch (symbol.type) {
            case INTEGER_LITERAL:
                return new IntegerLiteralExpression(symbol.value);
            case VARIABLE:
                return new IntegerVariableExpression(symbol.value);
            case KEYWORD:
                if (symbol.value.equals(Keyword.RANDOM.getCommand())) {
                    return (IntegerExpression) createRandomExpressionNode(Type.INTEGER_LITERAL, symbols);
                }
                throw new InvalidArgumentException("Only \"random\" keyword can be used here.");
            default:
                throw new IllegalParameterException(Type.INTEGER_LITERAL, symbol.type);
        }
    }

    private void consumeNewLine(Keyword keyword, List<Symbol> symbols) throws InvalidContinuationException {
        if (symbols.isEmpty() || symbols.get(0).type != Type.LINE_TERMINATOR) {
            throw new InvalidContinuationException(keyword);
        }

        symbols.remove(0);
    }

    ValueExpression createRandomExpressionNode(Type requiredType, List<Symbol> symbols) throws ParsingException {
        var symbolsAndType = ensureRandomParametersPresent(requiredType, symbols);
        return toRandomExpression(symbolsAndType.getFirst(), symbolsAndType.getSecond());
    }

    private Pair<List<Symbol>, Type> createSymbolList(Type requiredType, List<Symbol> symbols) throws ParsingException {
        Type previousType = null;
        var elementSymbols = new ArrayList<Symbol>();
        var iterator = symbols.iterator();

        while (iterator.hasNext()) {
            var symbol = iterator.next();
            if (symbol.type == Type.LIST_END) {
                iterator.remove();
                break;
            }

            ensureTypeValid(requiredType, symbol.type);

            if (previousType != null) {
                ensureTypeValid(previousType, symbol.type);
            }

            if (symbol.type != Type.VARIABLE) {
                previousType = symbol.type;
            }
            elementSymbols.add(symbol);
            iterator.remove();
        }
        return new Pair<>(elementSymbols, previousType);
    }

    private ValueExpression toRandomExpression(List<Symbol> elementSymbols, Type targetType) throws ParsingException {
        var expressionList = new ArrayList<ValueExpression>();
        for (var symbol : elementSymbols) {
            expressionList.add(toValueExpression(targetType, symbol));
        }

        if (expressionList.stream().allMatch(expression -> expression instanceof StringExpression)) {
            return new RandomStringExpression(expressionList.stream()
                    .map(e -> (StringExpression) e).collect(Collectors.toList()));
        } else if (expressionList.stream().allMatch(expression -> expression instanceof IntegerExpression)) {
            return new RandomIntegerExpression(expressionList.stream()
                    .map(e -> (IntegerExpression) e).collect(Collectors.toList()));
        } else {
            return new RandomValueExpression(expressionList.stream()
                    .map(e -> (VariableExpression) e).collect(Collectors.toList()));
        }
    }

    private ValueExpression toValueExpression(Type requiredType, Symbol symbol) throws ParsingException {
        switch (symbol.type) {
            case STRING_LITERAL:
                return new StringLiteralExpression(symbol.value);
            case INTEGER_LITERAL:
                return new IntegerLiteralExpression(symbol.value);
            case VARIABLE:
                if (requiredType == Type.STRING_LITERAL) {
                    return new StringVariableExpression(symbol.value);
                } else if (requiredType == Type.INTEGER_LITERAL) {
                    return new IntegerLiteralExpression(symbol.value);
                }
                return new VariableExpression(symbol.value);
            case IDENTIFIER:
                return new IdentifierExpression(symbol.value);
            default:
                return new VariableExpression(symbol.value);
        }
    }

    private void ensureTypeValid(Type requiredType, Type type) throws ParsingException {
        if (requiredType == null) return;

        switch (type) {
            case STRING_LITERAL:
            case INTEGER_LITERAL:
            case IDENTIFIER:
                if (type != requiredType) {
                    throw new ListTypeMismatchException(requiredType, type);
                }
                return;
            default:
                if (type != Type.VARIABLE) {
                    throw new IllegalListTypeParameter(type);
                }
        }
    }
}
