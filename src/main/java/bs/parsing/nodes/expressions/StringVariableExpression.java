package bs.parsing.nodes.expressions;

import bs.Environment;

public class StringVariableExpression implements StringExpression {
    private final String name;

    public StringVariableExpression(String name) {
        this.name = name;
    }

    public String getValue(Environment environment) {
        return environment.getStringVariable(name);
    }
}
