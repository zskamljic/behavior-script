package bs.parsing.nodes.expressions;

public class StringLiteralExpression implements StringExpression {
    private final String value;

    public StringLiteralExpression(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
