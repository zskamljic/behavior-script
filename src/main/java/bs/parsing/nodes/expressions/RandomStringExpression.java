package bs.parsing.nodes.expressions;

import bs.Environment;
import bs.parsing.nodes.expressions.util.RandomExpressionChooser;

import java.util.List;

public class RandomStringExpression extends StringVariableExpression {
    private List<StringExpression> stringExpressions;

    public RandomStringExpression(List<StringExpression> stringExpressions) {
        super(null);
        this.stringExpressions = stringExpressions;
    }

    @Override
    public String getValue(Environment environment) {
        return RandomExpressionChooser.evaluateRandomString(environment, stringExpressions);
    }
}
