package bs.parsing.nodes.expressions;

import bs.Environment;
import bs.parsing.nodes.expressions.util.RandomExpressionChooser;

import java.util.List;

public class RandomIntegerExpression extends IntegerVariableExpression {
    private List<IntegerExpression> integerExpressions;

    public RandomIntegerExpression(List<IntegerExpression> integerExpressions) {
        super(null);
        this.integerExpressions = integerExpressions;
    }

    @Override
    public int getValue(Environment environment) {
        return RandomExpressionChooser.evaluateRandomInteger(environment, integerExpressions);
    }
}
