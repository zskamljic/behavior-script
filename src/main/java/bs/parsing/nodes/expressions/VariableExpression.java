package bs.parsing.nodes.expressions;

import bs.Environment;

public class VariableExpression implements ValueExpression {
    private final String name;

    public VariableExpression(String name) {
        this.name = name;
    }

    public String getStringValue(Environment environment) {
        return environment.getStringVariable(name);
    }

    public int getIntegerValue(Environment environment) {
        return environment.getIntegerVariable(name);
    }
}
