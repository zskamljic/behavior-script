package bs.parsing.nodes.expressions;

public class IdentifierExpression implements ValueExpression {
    private final String value;

    public IdentifierExpression(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
