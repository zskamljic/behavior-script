package bs.parsing.nodes.expressions;

import bs.Environment;

public class IntegerVariableExpression implements IntegerExpression {
    private final String variableName;

    public IntegerVariableExpression(String variableName) {
        this.variableName = variableName;
    }

    public int getValue(Environment environment) {
        return environment.getIntegerVariable(variableName);
    }
}
