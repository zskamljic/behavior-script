package bs.parsing.nodes.expressions;

import bs.Environment;
import bs.parsing.nodes.expressions.util.RandomExpressionChooser;

import java.util.List;

public class RandomValueExpression implements ValueExpression {
    private List<ValueExpression> expressions;

    public RandomValueExpression(List<ValueExpression> expressions) {
        this.expressions = expressions;
    }

    public String getStringValue(Environment environment) {
        return RandomExpressionChooser.evaluateRandomString(environment, expressions);
    }

    public int getIntegerValue(Environment environment) {
        return RandomExpressionChooser.evaluateRandomInteger(environment, expressions);
    }
}
