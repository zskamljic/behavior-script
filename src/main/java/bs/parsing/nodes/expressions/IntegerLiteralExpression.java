package bs.parsing.nodes.expressions;

import bs.lexing.Type;
import bs.parsing.errors.IllegalParameterException;

public class IntegerLiteralExpression implements IntegerExpression {
    private final int value;

    public IntegerLiteralExpression(String value) throws IllegalParameterException {
        try {
            this.value = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new IllegalParameterException(Type.INTEGER_LITERAL, Type.STRING_LITERAL);
        }
    }

    public int getValue() {
        return value;
    }
}
