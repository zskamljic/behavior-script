package bs.parsing.nodes.expressions.util;

import bs.Environment;
import bs.parsing.nodes.expressions.*;

import java.util.List;

public class RandomExpressionChooser {
    private RandomExpressionChooser() {}

    public static <T extends ValueExpression> String evaluateRandomString(Environment environment, List<T> expressions) {
        var stringExpression = expressions.get(environment.getRandom(expressions.size()));

        if (stringExpression instanceof StringLiteralExpression) {
            return ((StringLiteralExpression) stringExpression).getValue();
        } else if (stringExpression instanceof StringVariableExpression) {
            return ((StringVariableExpression) stringExpression).getValue(environment);
        } else {
            throw new IllegalStateException("String can only be a variable or a literal");
        }
    }

    public static <T extends ValueExpression> int evaluateRandomInteger(Environment environment, List<T> expressions) {
        var expression = expressions.get(environment.getRandom(expressions.size()));

        if (expression instanceof IntegerLiteralExpression) {
            return ((IntegerLiteralExpression) expression).getValue();
        } else if (expression instanceof IntegerVariableExpression) {
            return ((IntegerVariableExpression) expression).getValue(environment);
        } else {
            throw new IllegalStateException("Integer must be either a literal or a variable");
        }
    }
}
