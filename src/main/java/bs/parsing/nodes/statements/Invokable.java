package bs.parsing.nodes.statements;

import bs.Environment;

import java.util.Map;

public interface Invokable {
    void execute(Environment environment, Map<String, StatementNode> methods);
}
