package bs.parsing.nodes.statements;

import bs.Environment;

import java.util.Map;

public class InvocationStatement implements StatementNode, Invokable {
    private final String methodName;

    public InvocationStatement(String methodName) {
        this.methodName = methodName;
    }

    @Override
    public void execute(Environment environment) {
        throw new RuntimeException("Must be given method candidates!");
    }

    @Override
    public void execute(Environment environment, Map<String, StatementNode> methods) {
        methods.get(methodName).execute(environment);
    }
}
