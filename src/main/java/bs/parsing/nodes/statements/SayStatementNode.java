package bs.parsing.nodes.statements;

import bs.Environment;
import bs.parsing.nodes.expressions.StringExpression;
import bs.parsing.nodes.expressions.StringLiteralExpression;
import bs.parsing.nodes.expressions.StringVariableExpression;

import java.security.InvalidParameterException;

public class SayStatementNode implements StatementNode {
    private final StringExpression valueExpression;

    public SayStatementNode(StringExpression valueExpression) {
        if ((valueExpression instanceof StringLiteralExpression ||
                valueExpression instanceof StringVariableExpression)) {
            this.valueExpression = valueExpression;
            return;
        }
        throw new InvalidParameterException("String value expression not valid");
    }

    @Override
    public void execute(Environment environment) {
        String value;
        if (valueExpression instanceof StringLiteralExpression) {
            value = ((StringLiteralExpression) valueExpression).getValue();
        } else if (valueExpression instanceof StringVariableExpression) {
            value = ((StringVariableExpression) valueExpression).getValue(environment);
        } else {
            throw new IllegalArgumentException("String value expression not valid");
        }
        environment.print(value);
    }
}
