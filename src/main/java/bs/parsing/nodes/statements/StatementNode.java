package bs.parsing.nodes.statements;

import bs.Environment;
import bs.parsing.nodes.AstNode;

public interface StatementNode extends AstNode {
    void execute(Environment environment);
}
