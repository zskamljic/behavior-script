package bs.parsing.nodes.statements;

import bs.Environment;

public class NoopStatement implements StatementNode {
    @Override
    public void execute(Environment environment) {
        // Needs to behave as a statement, but won't do anything
    }
}
