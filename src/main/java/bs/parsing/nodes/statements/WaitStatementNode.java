package bs.parsing.nodes.statements;

import bs.Environment;
import bs.parsing.nodes.expressions.IntegerLiteralExpression;
import bs.parsing.nodes.expressions.IntegerExpression;
import bs.parsing.nodes.expressions.IntegerVariableExpression;

import java.security.InvalidParameterException;

public class WaitStatementNode implements StatementNode {
    private final IntegerExpression valueExpression;

    public WaitStatementNode(IntegerExpression expression) {
        if (expression instanceof IntegerLiteralExpression ||
                expression instanceof IntegerVariableExpression) {
            valueExpression = expression;
            return;
        }
        throw new InvalidParameterException("Integer value expression not valid");
    }

    @Override
    public void execute(Environment environment) {
        int value;
        if (valueExpression instanceof IntegerLiteralExpression) {
            value = ((IntegerLiteralExpression) valueExpression).getValue();
        } else if (valueExpression instanceof IntegerVariableExpression) {
            value = ((IntegerVariableExpression) valueExpression).getValue(environment);
        } else {
            throw new IllegalArgumentException("Integer value expression not valid");
        }
        environment.wait(value);
    }
}
