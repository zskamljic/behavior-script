package bs.parsing.nodes.statements;

import bs.Environment;
import bs.parsing.nodes.expressions.IdentifierExpression;
import bs.parsing.nodes.expressions.ValueExpression;

import java.util.List;
import java.util.Map;

public class RandomInvocationStatement implements StatementNode, Invokable {
    private List<ValueExpression> expressions;

    public RandomInvocationStatement(List<ValueExpression> expressions) {
        this.expressions = expressions;
    }

    @Override
    public void execute(Environment environment) {
        throw new RuntimeException("Must be given method candidates!");
    }

    @Override
    public void execute(Environment environment, Map<String, StatementNode> methods) {
        var randomMethod = expressions.get(environment.getRandom(expressions.size()));
        if (!(randomMethod instanceof IdentifierExpression)) {
            throw new IllegalArgumentException("Parameter was not a function");
        }
        var identifierExpression = (IdentifierExpression) randomMethod;
        methods.get(identifierExpression.getValue()).execute(environment);
    }
}
