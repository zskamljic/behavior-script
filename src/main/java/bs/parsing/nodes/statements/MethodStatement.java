package bs.parsing.nodes.statements;

import bs.Environment;

import java.util.List;

public class MethodStatement implements StatementNode {
    private final String name;
    private final List<StatementNode> statements;

    public MethodStatement(String name, List<StatementNode> statements) {
        this.name = name;
        this.statements = statements;
    }

    @Override
    public void execute(Environment environment) {
        statements.forEach(node -> node.execute(environment));
    }

    public String getMethodName() {
        return name;
    }

    public List<StatementNode> getStatements() {
        return statements;
    }
}
