package bs;

import bs.lexing.Lexer;
import bs.lexing.errors.LexingException;
import bs.parsing.AbstractSyntaxTree;
import bs.parsing.errors.ParsingException;
import bs.parsing.nodes.statements.InvocationStatement;
import bs.parsing.nodes.statements.Invokable;
import bs.parsing.nodes.statements.MethodStatement;
import bs.parsing.nodes.statements.StatementNode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Executor {
    private final Map<String, StatementNode> methods = new HashMap<>();
    private final List<StatementNode> nodes;

    public Executor(String script) throws BehaviorScriptException {
        var lexer = new Lexer();
        try {
            var symbols = lexer.process(script);
            var treeBuilder = new AbstractSyntaxTree();
            nodes = treeBuilder.createTree(symbols);
            processMethods();
        } catch (LexingException | ParsingException e) {
            throw new BehaviorScriptException(e);
        }
    }

    public void execute(Environment environment) {
        nodes.forEach(node -> {
            if (node instanceof Invokable) {
                var invocation = (Invokable) node;
                invocation.execute(environment, methods);
            } else {
                node.execute(environment);
            }
        });
    }

    private void processMethods() throws ParsingException {
        for (StatementNode node : nodes) {
            if (node instanceof MethodStatement) {
                MethodStatement methodStatement = (MethodStatement) node;
                addUniqueMethod(methodStatement);
            }
        }

        nodes.removeIf(methods::containsValue);
    }

    private void addUniqueMethod(MethodStatement methodStatement) throws ParsingException {
        if (methods.containsKey(methodStatement.getMethodName())) {
            throw new ParsingException("More than one method defined with name " + methodStatement.getMethodName());
        }

        methods.put(methodStatement.getMethodName(), methodStatement);
    }
}
