package bs;

public class BehaviorScriptException extends Exception {
    public BehaviorScriptException(Exception e) {
        super(e);
    }
}
