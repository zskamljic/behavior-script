package bs;

public interface Environment {
    String getStringVariable(String name);

    int getIntegerVariable(String name);

    void print(String value);

    void wait(int value);

    int getRandom(int max);
}
