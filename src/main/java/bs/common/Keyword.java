package bs.common;

import java.util.Optional;
import java.util.stream.Stream;

public enum Keyword {
    RANDOM("random"),
    INVOKE("invoke"),
    SAY("say"),
    WAIT("wait");

    private final String command;

    Keyword(String command) {
        this.command = command;
    }

    public static boolean isKeyword(String lexeme) {
        return Stream.of(Keyword.values())
                .map(Keyword::getCommand)
                .anyMatch(command -> command.equals(lexeme));
    }

    public static Optional<Keyword> getByCommand(String command) {
        return Stream.of(values()).filter(keyword -> keyword.getCommand().equals(command)).findFirst();
    }

    public String getCommand() {
        return command;
    }

}
