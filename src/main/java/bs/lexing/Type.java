package bs.lexing;

public enum Type {
    CONTROL_TERMINATOR,
    INTEGER_LITERAL,
    KEYWORD,
    LINE_TERMINATOR,
    LIST_END,
    LIST_START,
    METHOD_DECLARATION,
    IDENTIFIER,
    STRING_LITERAL,
    VARIABLE
}
