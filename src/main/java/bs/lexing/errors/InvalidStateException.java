package bs.lexing.errors;

import bs.lexing.Lexer;

public class InvalidStateException extends LexingException {
    public InvalidStateException(Lexer.State state) {
        super("Invalid state: " + state);
    }
}
