package bs.lexing.errors;

public class MissingVariableNameException extends LexingException {
    public MissingVariableNameException(String message) {
        super(message);
    }
}
