package bs.lexing.errors;

public class InvalidNumberException extends LexingException {
    public InvalidNumberException(String value) {
        super(value + " could not be converted to a valid 32 bit integer");
    }
}
