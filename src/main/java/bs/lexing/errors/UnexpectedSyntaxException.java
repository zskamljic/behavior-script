package bs.lexing.errors;

public class UnexpectedSyntaxException extends LexingException {
    public UnexpectedSyntaxException(String message) {
        super("Syntax error: " + message);
    }
}
