package bs.lexing.errors;

public abstract class LexingException extends Exception {
    LexingException(String message) {
        super(message);
    }
}
