package bs.lexing;

import java.util.Objects;

public class Symbol {
    public final Type type;
    public final String value;

    public Symbol(Type type, String value) {
        this.type = type;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Symbol symbol = (Symbol) o;

        if (type != symbol.type) return false;
        return Objects.equals(value, symbol.value);
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
