package bs.lexing;

import bs.common.Keyword;
import bs.lexing.errors.InvalidNumberException;
import bs.lexing.errors.InvalidStateException;
import bs.lexing.errors.LexingException;
import bs.lexing.errors.MissingVariableNameException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Lexer {
    private static final String SPLIT_SPACE_NOT_IN_QUOTES = " +(?=((\\\\[\\\\\"]|[^\\\\\"])*\"(\\\\[\\\\\"]|[^\\\\\"])*\")*(\\\\[\\\\\"]|[^\\\\\"])*$)";
    private static final String SPLIT_COMMA_NOT_IN_QUOTES = ",+(?=((\\\\[\\\\\"]|[^\\\\\"])*\"(\\\\[\\\\\"]|[^\\\\\"])*\")*(\\\\[\\\\\"]|[^\\\\\"])*$)";
    private static final String METHOD_DECLARATION = "^[\\w]+:";

    private State state = State.DEFAULT;

    public List<Symbol> process(String script) throws LexingException {
        var symbols = new ArrayList<Symbol>();

        if (!script.endsWith("\n")) {
            script += "\n";
        }

        // Split on spaces, but not within quotes
        var split = createSplitList(script.split(SPLIT_SPACE_NOT_IN_QUOTES));

        while (!split.isEmpty()) {
            lexNext(split).ifPresent(symbols::add);
        }

        removeDuplicateNewlines(symbols);
        return symbols;
    }

    Optional<Symbol> lexNext(List<String> split) throws LexingException {
        if (split.isEmpty()) return Optional.empty();

        var lexeme = split.remove(0).replaceAll("^[\t ]+", "");
        switch (state) {
            case DEFAULT:
                return lexNextDefault(lexeme, split);
            case LIST:
                return lexNextList(lexeme, split);
            default:
                throw new InvalidStateException(state);
        }
    }

    private Optional<Symbol> lexNextDefault(String lexeme, List<String> split) throws LexingException {
        if (lexeme.startsWith("\"")) {
            return Optional.of(lexString(lexeme));
        }

        if (lexeme.startsWith("#")) {
            lexComment(split);
            return lexNext(split);
        }

        if (lexeme.startsWith("$")) {
            return Optional.of(lexVariable(lexeme));
        }

        if (lexeme.matches("-?\\d+")) {
            return Optional.of(lexInteger(lexeme));
        }

        if (lexeme.matches("\\n")) {
            return Optional.of(new Symbol(Type.LINE_TERMINATOR, null));
        }

        if (lexeme.startsWith("[")) {
            state = State.LIST;
            return Optional.of(lexListStart(lexeme, split));
        }

        if (lexeme.startsWith("]")) {
            return Optional.of(new Symbol(Type.LIST_END, null));
        }

        if (Keyword.isKeyword(lexeme)) {
            return Optional.of(new Symbol(Type.KEYWORD, lexeme));
        }

        if (lexeme.matches(METHOD_DECLARATION)) {
            return Optional.of(new Symbol(Type.METHOD_DECLARATION, lexeme.replaceAll("[^\\w]+", "")));
        }

        if ("end".equals(lexeme)) {
            return Optional.of(new Symbol(Type.CONTROL_TERMINATOR, lexeme));
        }

        if (lexeme.isBlank()) {
            return Optional.empty();
        }

        return Optional.of(new Symbol(Type.IDENTIFIER, lexeme));
    }

    private Optional<Symbol> lexNextList(String lexeme, List<String> split) throws LexingException {
        if (lexeme.endsWith(",")) {
            lexeme = lexeme.substring(0, lexeme.length() - 1);
        } else if (lexeme.contains(",")) {
            var parts = lexeme.split(SPLIT_COMMA_NOT_IN_QUOTES);
            lexeme = parts[0];
            if (parts.length > 1) {
                for (int i = parts.length - 1; i > 0; i--) {
                    split.add(0, parts[i]);
                }
            }
        } else if (lexeme.endsWith("]")) {
            lexeme = lexeme.substring(0, lexeme.length() - 1);
            split.add(0, "]");
            state = State.DEFAULT;
        }
        return lexNextDefault(lexeme, split);
    }

    private void lexComment(List<String> split) {
        var iterator = split.iterator();
        while (iterator.hasNext()) {
            var next = iterator.next();
            if (next.equals("\n")) break;

            iterator.remove();
        }
    }

    Symbol lexString(String lexeme) {
        return new Symbol(Type.STRING_LITERAL, lexeme.substring(1, lexeme.length() - 1));
    }

    Symbol lexInteger(String lexeme) throws InvalidNumberException {
        try {
            Integer.parseInt(lexeme);
            return new Symbol(Type.INTEGER_LITERAL, lexeme);
        } catch (NumberFormatException e) {
            throw new InvalidNumberException(lexeme);
        }
    }

    List<String> createSplitList(String[] values) {
        var list = new ArrayList<String>();

        for (var value : values) {
            if (value.contains("\n")) {
                addSplit(value, list);
                continue;
            }
            list.add(value);
        }

        return list;
    }

    void addSplit(String value, List<String> list) {
        var split = value.split("\r?\n");
        for (int i = 0; i < split.length; i++) {
            list.add(split[i]);
            if (i != split.length - 1 || value.endsWith("\n")) {
                list.add("\n");
            }
        }
    }

    Symbol lexVariable(String lexeme) throws MissingVariableNameException {
        if (lexeme.length() == 1) {
            throw new MissingVariableNameException("Variable must have a name, only $ given");
        }

        return new Symbol(Type.VARIABLE, lexeme.substring(1));
    }

    Symbol lexListStart(String lexeme, List<String> split) {
        if (lexeme.length() > 1) {
            var rest = lexeme.substring(1);
            split.add(0, rest);
        }

        return new Symbol(Type.LIST_START, null);
    }

    private void removeDuplicateNewlines(List<Symbol> symbols) {
        var iterator = symbols.iterator();

        Symbol previous = null;
        while (iterator.hasNext()) {
            var current = iterator.next();

            if (previous == null && current.type == Type.LINE_TERMINATOR || current.equals(previous)) {
                iterator.remove();
            }

            previous = current;
        }
    }

    public enum State {
        DEFAULT, LIST
    }
}
