package bs.lexing;

import bs.lexing.errors.InvalidNumberException;
import bs.lexing.errors.LexingException;
import bs.lexing.errors.MissingVariableNameException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class LexerVariablesTests {
    @Spy
    public Lexer lexer;

    @Test
    public void lexerRecognizesVariable() throws LexingException {
        var input = new ArrayList<>(List.of("$variable"));

        var result = lexer.lexNext(input).orElseThrow();

        assertEquals(Type.VARIABLE, result.type);
        assertEquals("variable", result.value);
    }

    @Test
    public void lexerRecognizesComplexVariableName() throws LexingException {
        var input = new ArrayList<>(List.of("$variable.with.path"));

        var result = lexer.lexNext(input).orElseThrow();

        assertEquals(Type.VARIABLE, result.type);
        assertEquals("variable.with.path", result.value);
    }

    @Test(expected = MissingVariableNameException.class)
    public void lexVariableThrowsWithoutName() throws MissingVariableNameException {
        lexer.lexVariable("$");
    }
}
