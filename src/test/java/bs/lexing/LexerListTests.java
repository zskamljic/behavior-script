package bs.lexing;

import bs.lexing.errors.LexingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class LexerListTests {
    @Spy
    private Lexer lexer;

    @Test
    public void lexNextReturnsListSingleString() throws LexingException {
        var input = new ArrayList<>(List.of("[\"a\"]"));

        var start = lexer.lexNext(input).orElseThrow();
        var value = lexer.lexNext(input).orElseThrow();
        var end = lexer.lexNext(input).orElseThrow();

        assertEquals(Type.LIST_START, start.type);
        assertEquals(Type.STRING_LITERAL, value.type);
        assertEquals(Type.LIST_END, end.type);
    }

    @Test
    public void lexNextReturnsListSingleInteger() throws LexingException {
        var input = new ArrayList<>(List.of("[1]"));

        var start = lexer.lexNext(input).orElseThrow();
        var value = lexer.lexNext(input).orElseThrow();
        var end = lexer.lexNext(input).orElseThrow();

        assertEquals(Type.LIST_START, start.type);
        assertEquals(Type.INTEGER_LITERAL, value.type);
        assertEquals(Type.LIST_END, end.type);
    }

    @Test
    public void lexNextReturnsListSingleVariable() throws LexingException {
        var input = new ArrayList<>(List.of("[$1]"));

        var start = lexer.lexNext(input).orElseThrow();
        var value = lexer.lexNext(input).orElseThrow();
        var end = lexer.lexNext(input).orElseThrow();

        assertEquals(Type.LIST_START, start.type);
        assertEquals(Type.VARIABLE, value.type);
        assertEquals(Type.LIST_END, end.type);
    }

    @Test
    public void processListOfStrings() throws LexingException {
        var input = "[\"a\", \"b\",\"c\"]";

        var output = lexer.process(input);

        assertEquals(6, output.size());
        assertEquals(Type.LIST_START, output.get(0).type);
        assertEquals(Type.LIST_END, output.get(output.size() - 2).type);

        for (int i = 1; i < output.size() - 2; i++) {
            assertEquals(Type.STRING_LITERAL, output.get(i).type);
        }
    }

    @Test
    public void processListOfIntegers() throws LexingException {
        var input = "[1, 2,3]";

        var output = lexer.process(input);

        assertEquals(6, output.size());
        assertEquals(Type.LIST_START, output.get(0).type);
        assertEquals(Type.LIST_END, output.get(output.size() - 2).type);

        for (int i = 1; i < output.size() - 2; i++) {
            assertEquals(Type.INTEGER_LITERAL, output.get(i).type);
        }
    }

    @Test
    public void processListOfVariables() throws LexingException {
        var input = "[$1, $2,$3]";

        var output = lexer.process(input);

        assertEquals(6, output.size());
        assertEquals(Type.LIST_START, output.get(0).type);
        assertEquals(Type.LIST_END, output.get(output.size() - 2).type);

        for (int i = 1; i < output.size() - 2; i++) {
            assertEquals(Type.VARIABLE, output.get(i).type);
        }
    }

    @Test
    public void lexListStartReturnsListStartOnly() {
        var result = lexer.lexListStart("[", List.of());

        assertNotNull(result);
        assertEquals(Type.LIST_START, result.type);
    }

    @Test
    public void lexListStartReturnsStartAndPrependsRest() {
        var list = new ArrayList<String>();
        var result = lexer.lexListStart("[1", list);

        assertNotNull(result);
        assertEquals(Type.LIST_START, result.type);

        assertEquals(1, list.size());
        assertEquals("1", list.get(0));
    }
}