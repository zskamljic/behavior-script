package bs.lexing;

import bs.lexing.errors.InvalidNumberException;
import bs.lexing.errors.LexingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class LexerCommentsTests {
    @Spy
    private Lexer lexer;

    @Test
    public void lexNextRemovesStartComment() throws LexingException {
        var input = new ArrayList<>(List.of("#", "some", "other", "words", "\n"));

        var result = lexer.lexNext(input);

        assertTrue(result.isPresent());
        assertEquals(Type.LINE_TERMINATOR, result.get().type);
        assertEquals(0, input.size());
    }

    @Test
    public void lexNextRemovesCommentAfterStatement() throws LexingException {
        var input = new ArrayList<>(List.of("say", "#", "some", "\n"));

        var saySymbol = lexer.lexNext(input);

        assertNotNull(saySymbol);

        var result = lexer.lexNext(input);

        assertTrue(result.isPresent());
        assertEquals(Type.LINE_TERMINATOR, result.get().type);
        assertEquals(0, input.size());
    }

    @Test
    public void lexNextRemovesCommentWithoutSpace() throws LexingException {
        var input = new ArrayList<>(List.of("say", "#some", "\n"));

        var saySymbol = lexer.lexNext(input);

        assertNotNull(saySymbol);

        var result = lexer.lexNext(input);

        assertTrue(result.isPresent());
        assertEquals(Type.LINE_TERMINATOR, result.get().type);
        assertEquals(0, input.size());
    }
}
