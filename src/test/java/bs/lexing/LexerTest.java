package bs.lexing;

import bs.lexing.errors.InvalidNumberException;
import bs.lexing.errors.LexingException;
import bs.lexing.errors.UnexpectedSyntaxException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LexerTest {
    @Spy
    private Lexer lexer;

    @Test
    public void processSplitsAndLexes() throws LexingException {
        doAnswer(invocation -> {
            var list = (List) invocation.getArgument(0);
            list.remove(0);
            return Optional.empty();
        }).when(lexer).lexNext(any());

        lexer.process("a \"b\tc\"\nd");
        verify(lexer, times(5)).lexNext(any());
    }

    @Test
    public void processHandlesCrLf() throws LexingException {
        var symbols = lexer.process("say\r\nwait");

        assertEquals(4, symbols.size());
    }

    @Test
    public void processCreatesTerminatedListWithoutTrailingNewline() throws LexingException {
        var symbols = lexer.process("say \"hello\"");

        assertEquals(3, symbols.size());

        var lastSymbol = symbols.get(symbols.size() - 1);
        assertEquals(Type.LINE_TERMINATOR, lastSymbol.type);
        assertNull(lastSymbol.value);
    }

    @Test
    public void processCreatesTerminatedListWithTrailingNewline() throws LexingException {
        var symbols = lexer.process("say \"hello\"\n");

        assertEquals(3, symbols.size());

        var lastSymbol = symbols.get(symbols.size() - 1);
        assertEquals(Type.LINE_TERMINATOR, lastSymbol.type);
        assertNull(lastSymbol.value);
    }

    @Test
    public void lexNextDelegatesToLexString() throws LexingException {
        var list = new ArrayList<>(List.of("\"abc cde def\"", "efg"));
        var result = lexer.lexNext(list);

        verify(lexer).lexString(anyString());

        assertNotNull(result);
        assertTrue(result.isPresent());

        var value = result.get();
        assertEquals(Type.STRING_LITERAL, value.type);
        assertEquals("abc cde def", value.value);
    }

    @Test
    public void lexStringTakesOnlyTerminated() {
        var result = lexer.lexString("\"abc\"");

        assertEquals("abc", result.value);
    }

    @Test
    public void lexNextCreatesIntegerLiteral() throws LexingException {
        var list = new ArrayList<>(Collections.singletonList("123"));
        var result = lexer.lexNext(list);

        assertNotNull(result);
        assertTrue(result.isPresent());

        var value = result.get();
        assertEquals(Type.INTEGER_LITERAL, value.type);
        assertEquals("123", value.value);
    }

    @Test
    public void lexNextCreatesNegativeIntegerLiteral() throws LexingException {
        var list = new ArrayList<>(Collections.singletonList("-123"));
        var result = lexer.lexNext(list);

        assertNotNull(result);
        assertTrue(result.isPresent());

        var value = result.get();
        assertEquals(Type.INTEGER_LITERAL, value.type);
        assertEquals("-123", value.value);
    }

    @Test(expected = InvalidNumberException.class)
    public void lexIntegerFailsWithLong() throws InvalidNumberException {
        var longString = String.valueOf(Long.MAX_VALUE);
        lexer.lexInteger(longString);
    }

    @Test
    public void lexIntegerSucceedsWithNegative() throws InvalidNumberException {
        var result = lexer.lexInteger("-123");

        assertNotNull(result);
        assertEquals(Type.INTEGER_LITERAL, result.type);
        assertEquals("-123", result.value);
    }

    @Test
    public void createSplitListNoSpecialCases() {
        var result = lexer.createSplitList(new String[]{"a", "b", "c"});

        assertEquals(List.of("a", "b", "c"), result);
    }

    @Test
    public void createSplitListWithTrailingNewline() {
        var result = lexer.createSplitList(new String[]{"a", "b\n"});

        assertEquals(List.of("a", "b", "\n"), result);
    }

    @Test
    public void createSplitListWithContainingNewLine() {
        var result = lexer.createSplitList(new String[]{"a", "b\nc"});

        assertEquals(List.of("a", "b", "\n", "c"), result);
    }

    @Test
    public void addSplit() {
        var list = new ArrayList<String>();

        lexer.addSplit("a\nb\nc", list);

        assertEquals(5, list.size());
    }

    @Test
    public void sayIsKeyword() throws LexingException {
        var result = lexer.process("say");

        assertEquals(2, result.size());

        var symbol = result.get(0);
        assertEquals("say", symbol.value);
        assertEquals(Type.KEYWORD, symbol.type);
    }

    @Test
    public void detectsMethodDeclaration() throws LexingException {
        var result = lexer.process("method:");

        assertEquals(2, result.size());

        var symbol = result.get(0);
        assertEquals("method", symbol.value);
        assertEquals(Type.METHOD_DECLARATION, symbol.type);
    }

    @Test
    public void detectsControlTerminator() throws LexingException {
        var result = lexer.process("end");

        assertEquals(2, result.size());

        var symbol = result.get(0);
        assertEquals(Type.CONTROL_TERMINATOR, symbol.type);
    }
}