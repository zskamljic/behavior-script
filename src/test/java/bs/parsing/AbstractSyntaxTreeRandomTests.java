package bs.parsing;

import bs.lexing.Symbol;
import bs.lexing.Type;
import bs.parsing.errors.InvalidParameterCountException;
import bs.parsing.errors.ListTypeMismatchException;
import bs.parsing.errors.ParsingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class AbstractSyntaxTreeRandomTests {
    @Spy
    private AbstractSyntaxTree syntaxTree;

    @Test(expected = InvalidParameterCountException.class)
    public void randomRequiresList() throws ParsingException {
        var symbols = new ArrayList<>(List.of(new Symbol(Type.KEYWORD, "random")));

        syntaxTree.createKeywordStatement(symbols);
    }

    @Test
    public void createRandomExpressionNodeConsumesList() throws ParsingException {
        var symbols = new ArrayList<>(
                List.of(
                        new Symbol(Type.LIST_START, null),
                        new Symbol(Type.VARIABLE, null),
                        new Symbol(Type.LIST_END, null)
                )
        );

        syntaxTree.createRandomExpressionNode(null, symbols);

        assertEquals(0, symbols.size());
    }

    @Test(expected = ListTypeMismatchException.class)
    public void createRandomExpressionThrowsOnRequiredMismatch() throws ParsingException {
        var symbols = new ArrayList<>(
                List.of(
                        new Symbol(Type.LIST_START, null),
                        new Symbol(Type.STRING_LITERAL, null)
                )
        );

        syntaxTree.createRandomExpressionNode(Type.INTEGER_LITERAL, symbols);
    }

    @Test(expected = ListTypeMismatchException.class)
    public void createRandomExpressionThrowsOnMismatch() throws ParsingException {
        var symbols = new ArrayList<>(
                List.of(
                        new Symbol(Type.LIST_START, null),
                        new Symbol(Type.INTEGER_LITERAL, null),
                        new Symbol(Type.STRING_LITERAL, null),
                        new Symbol(Type.LIST_END, null),
                        new Symbol(Type.LINE_TERMINATOR, null)
                )
        );

        syntaxTree.createRandomExpressionNode(null, symbols);
    }
}