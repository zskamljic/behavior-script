package bs.parsing;

import bs.lexing.Symbol;
import bs.lexing.Type;
import bs.parsing.errors.ParsingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AbstractSyntaxTreeMethodTests {
    @Spy
    private AbstractSyntaxTree tree;

    @Test
    public void createStatementCreatesMethod() throws ParsingException {
        var symbols = new ArrayList<>(List.of(
                new Symbol(Type.METHOD_DECLARATION, "method"),
                new Symbol(Type.CONTROL_TERMINATOR, null)
        ));

        tree.createStatement(symbols);
        verify(tree).createMethodStatement(symbols);
    }

    @Test
    public void createMethodStatementReturnsMethodStatement() throws ParsingException {
        var symbols = new ArrayList<>(List.of(
                new Symbol(Type.METHOD_DECLARATION, "method"),
                new Symbol(Type.CONTROL_TERMINATOR, null)
        ));

        var result = tree.createMethodStatement(symbols);
        assertNotNull(result);
        assertEquals("method", result.getMethodName());
        assertEquals(0, result.getStatements().size());
    }
}
