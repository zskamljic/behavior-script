package bs.parsing.nodes.expressions;

import bs.parsing.errors.IllegalParameterException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IntegerLiteralExpressionTest {
    @Test
    public void succeedsOnIntegerInput() throws IllegalParameterException {
        var result = new IntegerLiteralExpression("5");
        assertEquals(5, result.getValue());
    }

    @Test(expected = IllegalParameterException.class)
    public void failsOnNullInput() throws IllegalParameterException {
        new IntegerLiteralExpression(null);
    }

    @Test(expected = IllegalParameterException.class)
    public void failsOnNonIntegerString() throws IllegalParameterException {
        new IntegerLiteralExpression("abc");
    }
}