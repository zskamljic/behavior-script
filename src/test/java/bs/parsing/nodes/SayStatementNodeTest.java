package bs.parsing.nodes;

import bs.Environment;
import bs.parsing.nodes.expressions.StringVariableExpression;
import bs.parsing.nodes.statements.SayStatementNode;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class SayStatementNodeTest {
    @Test
    public void executeTakesVariableValue() {
        var node = new SayStatementNode(new StringVariableExpression("name"));

        var environment = mock(Environment.class);
        doReturn("value").when(environment).getStringVariable(any());

        node.execute(environment);

        verify(environment).getStringVariable("name");
        verify(environment).print("value");
    }
}