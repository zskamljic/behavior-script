package bs.parsing.nodes;

import bs.Environment;
import bs.parsing.nodes.expressions.IntegerVariableExpression;
import bs.parsing.nodes.statements.WaitStatementNode;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class WaitStatementNodeTest {
    @Test
    public void executeRequiresVariable() {
        var node = new WaitStatementNode(new IntegerVariableExpression("name"));

        var environment = mock(Environment.class);
        doReturn(5).when(environment).getIntegerVariable(any());

        node.execute(environment);

        verify(environment).getIntegerVariable("name");
        verify(environment).wait(5);
    }
}