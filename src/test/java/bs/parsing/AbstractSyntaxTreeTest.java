package bs.parsing;

import bs.lexing.Symbol;
import bs.lexing.Type;
import bs.parsing.errors.InvalidContinuationException;
import bs.parsing.errors.InvalidParameterCountException;
import bs.parsing.errors.ParsingException;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(MockitoJUnitRunner.class)
public class AbstractSyntaxTreeTest {
    @Spy
    private AbstractSyntaxTree syntaxTree;

    @Test(expected = InvalidParameterCountException.class)
    public void describesErrorWithoutKeywordParameter() throws ParsingException {
        var symbols = new ArrayList<>(List.of(new Symbol(Type.KEYWORD, "say")));

        syntaxTree.createKeywordStatement(symbols);
    }

    @Test(expected = InvalidContinuationException.class)
    public void createSayThrowsWithoutTermination() throws ParsingException {
        var symbols = new ArrayList<>(List.of(
                new Symbol(Type.STRING_LITERAL, "")
        ));

        syntaxTree.createSayStatementNode(symbols);
    }

    @Test
    public void createSayWorksWithTerminator() throws ParsingException {
        var symbols = new ArrayList<>(List.of(
                new Symbol(Type.STRING_LITERAL, ""),
                new Symbol(Type.LINE_TERMINATOR, null)
        ));

        var result = syntaxTree.createSayStatementNode(symbols);
        assertNotNull(result);
    }

    @Test
    public void createSayWorksWithVariable() throws ParsingException {
        var symbols = new ArrayList<>(List.of(
                new Symbol(Type.VARIABLE, ""),
                new Symbol(Type.LINE_TERMINATOR, null)
        ));

        var result = syntaxTree.createSayStatementNode(symbols);
        assertNotNull(result);
    }

    @Test
    public void createWaitWorksWithVariable() throws ParsingException {
        var symbols = new ArrayList<>(List.of(
                new Symbol(Type.VARIABLE, ""),
                new Symbol(Type.LINE_TERMINATOR, null)
        ));

        var result = syntaxTree.createWaitStatementNode(symbols);
        assertNotNull(result);
    }
}