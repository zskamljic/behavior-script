package bs.running;

import bs.Environment;

public class TestEnvironment implements Environment {
    private StringBuilder outputBuilder = new StringBuilder();

    String getFinalState() {
        return outputBuilder.toString();
    }

    @Override
    public String getStringVariable(String name) {
        return "Variable(\"" + name + "\")";
    }

    @Override
    public int getIntegerVariable(String name) {
        return 108;
    }

    @Override
    public void print(String value) {
        outputBuilder.append("Saying: ").append(value).append("\n");
    }

    @Override
    public void wait(int value) {
        outputBuilder.append("Waiting: ").append(value).append("\n");
    }

    @Override
    public int getRandom(int max) {
        return 0;
    }
}
