package bs.running;

import bs.BehaviorScriptException;
import bs.Executor;
import bs.lexing.errors.LexingException;
import bs.parsing.errors.ParsingException;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

public class RunnerTest {
    @Test
    public void run() throws IOException, BehaviorScriptException {
        var content = Files.readString(Paths.get("test.bs"));
        var expectedFinalState = Files.readString(Paths.get("test-output.txt"));

        var environment = new TestEnvironment();
        var executor = new Executor(content);
        executor.execute(environment);

        var finalState = environment.getFinalState();
        assertEquals(expectedFinalState, finalState);
    }
}