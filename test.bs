# comment for the whole file
say "Hello, world!" # comment explaining what the line does
wait 5
say "Goodbye, world!" #comment with no space at start
say $some.variable
wait $some.value
random ["a", "b", "c"]
random [1, 2, 3]
random [$1, $2, $3]
random ["a", $b, $c]
random [1, $2, $3]
wait random [1,2,3]
say random ["a", "b", "c"]
method:
    say "I'm in a method!"
end
invoke method
saya:
    say "SAY"
end
waita:
    say "WAIT"
end
invoke random [saya, method, waita]