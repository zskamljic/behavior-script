# Behavior Script (BS)

Behavior Script or BS for short is a simple interpreted language intended for use when scripting bots.

## Functionality

The language can be run by creating an instance of Executor class. The class takes single parameter of type Environment.

Environment is used to handle requested delays and printing.

### Documentation

The language supports the following keywords:

- [invoke](#invoke-keyword)
- [random](#random-keyword)
- [say](#say-keyword)
- [wait](#wait-keyword)

#### Invoke keyword

Invokes a method that has been defined anywhere in file. Can be combined with random.

Syntax:

```
invoke method
invoke random [methodA, methodB, methodC]
```

#### Random keyword

Selects a random value from provided list. List can be of any type, but will be required to be
of a correct type if used with other keywords or methods.

Syntax:

```
random ["a", "b", "c"] # picks randomly between "a", "b" and "c"
random [1, 2, 3] # picks one of the values 1, 2 or 3
random [$1, $2, $3] # picks one of the variables, but does not resolve the value yet
random [methodA, methodB, methodC] # picks one of the methods at random, does not execute it yet
```

Random will do nothing if used directly, it's value must be consumed by a keyword. Example:

```
say random ["Hello", "Goodbye"] # Says either "Hello" or "Goodbye"
wait random [5, 10] # waits for either 5 or 10 units
```

#### Say keyword

Prints the specified string to output defined in Environment. Parameter must be a valid string or variable of type string.
Can be combined with random.

Syntax:
```
say "Hello world!"
say random ["A", "B", "C"]
```

#### Wait keyword

Waits for a time period depending on implementation of Environment. Parameter must be an integer value or variable.
Can be combined with random.

Syntax:
```
wait 5
wait random [1, 2, 3]
```

#### Comments 

Comments can be written by using '#' prefix, i.e.:

```
# this is a comment
#this too is a comment
say "Hello" # this is a comment for the statement 
```

#### Variables

Variables can be used in scripts. An example of variable being used is: `$variable`.
Only condition for variables is that they must be prefixed with $ and have a length of more than one character, without spaces.

Variables are retrieved from Environment object, based on their type. 
Environment should throw `NoSuchElementException` if value for given name could not be found.

#### Lists

BS also supports lists. Lists need to have an uniform type, i.e. you cannot use strings and 
integers in the same list (variables can be used alongside literals).

Lists are strongly typed. That means if a list is a list of variables their type will be inferred from usage, i.e. 
if list is used where string values are required they will be retrieving string values.

List type is automatically inferred to be of a certain type at place of declaration 
(as well as loading their values from Environment) if
it contains at least one literal value (at least one string literal will result in a list of strings, 
at least one integer will result in a list of integers).

Sample declarations of a list:

```
[1, 2, 3] # list of integers
["a", "b", "c"] # list of strings
[$a, $b, $c] # list of variables, type will be infered when used (values not loaded until used)
[1, $b, $c] # infered as list of integers, loaded from environment immediately
["a", $b, $c] # infered as list of strings, loaded from environment immediately
["a", 2, $c] # illegal, list of mixed types
[methodA, methodB, methodC] # list of methods
```

#### Methods

The language also supports definition of methods. Methods can be used by using the `invoke` keyword, i.e. `invoke method`.

Methods can be defined as follows:

```
method:
    say "I'm a method!"
end
```

Where 'method' is the method name, the method body is terminated by 'end'. Only one method can be defined for a given
name. Names may only contain alphabetic characters.